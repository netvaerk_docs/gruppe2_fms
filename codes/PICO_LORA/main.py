import time
from time import sleep
from machine import Pin, SPI, I2C, SoftI2C, PWM
from lora import SX1262
from bme680 import *
import ssd1306
from ssd1306 import SSD1306
import json

device_id = "1"

buzzer = PWM(Pin(16))
reed = machine.Pin(5, machine.Pin.IN, machine.Pin.PULL_UP)
hall = machine.Pin(13, machine.Pin.IN)
reed_door = ""
reed_door2 = ""
reed_door_dk = ""

# første argument er valg af i2c peripheral på pico (0 eller 1).
i2c = I2C(1, sda = Pin(6), scl = Pin(7), freq = 400000)
bme = BME680_I2C(i2c=i2c) #(i2c, 119)
oled_width = 128
oled_height = 64
oled = ssd1306.SSD1306_I2C(oled_width, oled_height, i2c)

# Settings til lora sx1262 med Raspberry Pi Pico med lora HAT fra waveshare sx1262
def get_modem():
    lora_cfg = { 'freq_khz': 875000, # 868000 for europæisk 868Mhz bånd
                 'sf': 7, # Spreading factor
                 'coding_rate': 5, # 4/5 coding
                 'bw': '125', # båndbredde
                 }   
    
# Required! spi & chip select.
    spi = SPI(1, baudrate=2000_000, sck=Pin(10), mosi=Pin(11), miso=Pin(12))
    cs = Pin(3) # Required

    return SX1262(spi, cs,
                 busy=Pin(2),  # Required
                 dio1=Pin(20),   # Optional, recommended
                 dio2_rf_sw=True, # Automatisk switching af antennes receiver/transmitter tilstand
                 dio3_tcxo_millivolts=1700, # 1700 per recommendation from datasheet
                 reset=Pin(15),  # Optional, recommended
                 lora_cfg=lora_cfg)

#OBS - pin 26 kan anvendes til battery-detection. Ask How!

# Modulet henter målinger fra BME680 og sensorer ved dør, pakker en json fil og sender. Blink angiver aktiv
def play_tone(frequency):
    # Set maximum volume
    buzzer.duty_u16(1000)
    # Play tone
    buzzer.freq(frequency)

def be_quiet():
    # Set minimum volume
    buzzer.duty_u16(0)
    ## Infinite loop

# Tegn en halvåben dør
def draw_half_open_door():
    oled.fill(0)
    
    # Dørkarmen (rektangel)
    oled.rect(20, 20, 30, 60, 1)  # Gør døren mindre (mindre bredde og højde)

    # Dør (delvist åbent rektangel, vendt)
    oled.line(20, 20, 10, 40, 1)  # Øverste skrå linje (ændret for mindre dør og vendt)
    oled.line(10, 40, 10, 80, 1)  # Vertikal linje (ændret for mindre dør og vendt)
    oled.line(20, 50, 10, 80, 1)  # Nederste skrå linje (ændret for mindre dør og vendt)

    # Tegn dørhåndtaget (justeret position)
    oled.pixel(15, 50, 1)
    
    # Opdater skærmen for at vise tegningen
    oled.show()

def main():
    print("Initializing...")
    modem = get_modem()
    
    while True:
        try:
            # Er der liv? Onboard LED blink når der tages en måling
            led = Pin(25, Pin.OUT)
            led.on()
            sleep(0.2)
            led.off()
            sleep(0.2)
            
            # Måleværdier temperatur, luftfugtighed, lufttryk, gasser.
            temp = round(bme.temperature, 2)
            humi = round(bme.humidity, 2)
            pres = round(bme.pressure, 2)
            gas = round(bme.gas/1000, 2)
            
            # Reedsensor og hallsensor - er døren åben eller lukket.
            if reed.value() == 0:
                print("Reed: Dør SKAB 1 er lukket")
                reed_door = "Skab 1 closed"
                reed_door_dk = "Skab 1 lukket"
                #oled.fill(0)
                #oled.show()
            elif reed.value() == 1:
                print("Reed: Dør SKAB 1 er åben")
                reed_door = "Skab 1 open"
                #reed_door_dk = "Skab 1 åben"
                draw_half_open_door()
                play_tone(65)
                sleep(0.05)
                play_tone(105)
                sleep(0.05)
                play_tone(246)
                sleep(0.10)
                be_quiet()
                oled.fill(0)
                oled.show()
                #sleep(3)
# OBS. hall.value er byttet rundt, da sensor ikke virker korrekt, men viser konstant 1
            if hall.value() == 0:
                print("Hall: Dør SKAB 1 er lukket")
            elif hall.value() == 1:   
                print("Hall: Dør SKAB 1 er åben")
                play_tone(65)
                sleep(0.05)
                play_tone(105)
                sleep(0.05)
                play_tone(246)
                sleep(0.10)
                be_quiet()
                #sleep(3)
            
            #print værdier i REPL
            print('Temperature:', temp)
            print('Humidity:', humi)
            print('Pressure:', pres)
            print('Gas:', gas)
            print('-------')
        except OSError as e:
            print('Failed to read sensor.')
      
        # Datastreng        
        sensor_data = {
        "device": {"device_id": str(device_id)},
        "temp": temp,
        "humi": humi,
        "pres": pres,
        "gas": gas,
        #"door": reed_door_dk
        }
        
        #Pak og send datastreng
        data1 = json.dumps(sensor_data)
        print(data1)
        
        print("Sending...")
        modem.send(data1.encode())
        
        print("Sent!")
               
        #Snup en pause inden næste opsamling
        time.sleep(1) # Ved test, data ofte - ellers mindst 60 min.

if __name__ == "__main__":
    main()
