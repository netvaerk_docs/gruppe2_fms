import time
from machine import Pin, I2C

import AHT10

# Create I2C object
sda = Pin(6)
scl = Pin(7)
i2c = I2C(1, scl=scl, sda=sda)

# Create the sensor object using I2C
sensor = AHT10.AHT10(i2c)

temperature = round(sensor.temperature, 1)
humidity = round(sensor.relative_humidity, 1)

while True:
    print("Temperature: ", temperature, "C")
    print("Humidity: ", humidity, "%")
    print()
    time.sleep(5)