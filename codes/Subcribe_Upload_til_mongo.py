import paho.mqtt.client as mqtt
import socket
import time
import json
import datetime
import uuid
import pymongo

'''MQTT Info'''
broker = '172.20.10.5'
topic = 'topic'

'''Mongo DB Info'''
name = "Målinger"
host = "mongodb://localhost:27017/"
#device_id_0 = uuid.uuid4()
device_id_0 = "71adde2a-edf2-4f8d-9985-b5699ba50aad"
device_id_1 = "a8efce1b-ffe1-4ebd-9bf2-43fbce670427"

'''Mongo DB Metoder'''

def database(dbname: str, dbhost: str) -> pymongo.database.Database:
    client = pymongo.MongoClient(dbhost)
    mydb = client[dbname]
    return mydb

def insertdata(db: pymongo.database.Database, collection: str, data: dict) -> None:
    col = db[collection]
    col.insert_one(data)

''' MQTT Metoder'''

def on_connect(client, userdata, flags, rc, properties):
    print("Connected with result code " + str(rc))

def on_publish(client, userdata, mid):
    print("Data published \n")

''' Mongo DB defination'''
db = database(name, host)
print(db.name + " exists")  

'''Uploader kode til MongoDB'''

def on_message(client, userdata, message):
    timestamp = datetime.datetime.now()
    try:
        x = json.loads(message.payload.decode("utf-8"))    
        y= str((timestamp.day))+ "/" + str(timestamp.month)+ "/"+ str((timestamp.year))+ " " + str((timestamp.hour))+ ":" + str((timestamp.minute))+ ":" + str((timestamp.second))
        x["timestamp"] = y
        print(x)
        insertdata(db, "sensor_readings", x)
    except Exception as e:
        print("fejl" , e)


client = mqtt.Client(mqtt.CallbackAPIVersion.VERSION2)
print("connecting to broker")
client.connect(broker, port=1883)
client.on_connect = on_connect
client.on_message = on_message

client.loop_start()

while True:
    print("Subscribing to topic", topic)
    client.subscribe(topic)
    client.on_message
    time.sleep(5)