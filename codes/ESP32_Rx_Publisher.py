import time
from machine import Pin, SPI, I2C
from lora import SX1262
import utime
import aht10
import json

from umqtt.simple import MQTTClient
import machine 
import ubinascii 
import network
from time import sleep

#Esp-32 del

ssid = b'IP2'
password = 'fuckjerLudere69'
broker = '172.20.10.5'

client_id = ubinascii.hexlify(machine.unique_id())
topic_pub = b'topic'

station = network.WLAN(network.STA_IF)
station.active(True)

if station.isconnected() == False:
    station.connect(ssid, password)

print('Connection successful')
time.sleep(3)

client = MQTTClient(client_id, broker)
client.connect()

#Lora Del
'''Med ESP-32-v1(15pins) - Brug følgende pin: spi= 2, sck= 18, mosi= 23, miso= 19,
cs= 15, busy= 5, dio1= 22, reset= 4'''

def get_modem():
    lora_cfg = { 'freq_khz': 875000, # 868000 for europæisk 868Mhz bånd
                 'sf': 7, # Spreading factor
                 'coding_rate': 5, # 4/5 coding
                 'bw': '125', # båndbredde
                 }   
    
    '''HSPI: id=1 - SCK 14, MOSI 13, MISO 12
    VSPI: id=2 - SCK 18, MOSI 23, MISO 19'''
    spi = SPI(1, baudrate=2000_000, sck=Pin(14), mosi=Pin(13), miso=Pin(12)) # Required.
    #spi = SPI(2, baudrate=2000_000, sck=Pin(18), mosi=Pin(23), miso=Pin(19))
    cs = Pin(5) # Required

    return SX1262(spi, cs,
                 busy=Pin(2),  # Required
                 dio1=Pin(17),   # Optional, recommended
                 dio2_rf_sw=True, # Automatisk switching af antennes receiver/transmitter tilstand
                 dio3_tcxo_millivolts=1700, # 1700 per recommendation from datasheet
                 reset=Pin(15),  # Optional, recommended
                 lora_cfg=lora_cfg)

i2c = I2C(1,scl=Pin(22), sda=Pin(21), freq=400000)
sensor = aht10.AHT10(i2c)

device_id_0 = "1"

#modtager

def main():
    print("Initializing...")
    modem = get_modem()

    while True:
        print("Receiving...")
        rx = modem.recv(timeout_ms=1000) # timeout for hvor lang tid der må gå før den giver op
        
        if rx:
#            print((f'{rx}'))#[12:45])
            msg = (f'{rx}')
            msg1 = msg.replace("bytearray(b'",'').replace("')", '')
            
            decoded = json.loads(msg1)
            print(decoded)
            msgPublish = decoded
            client.publish(topic_pub, msgPublish)
#            decoded = msg.decode()
#            print(type(decoded))
        else:
            print("Timeout!")
#         print(rx)
        time.sleep(1)
        
'''

#Afsender

def main():
    print("Initializing...")
    modem = get_modem()

    while True:
        tempData1 = round(sensor.temperature,1)
        humiData1 = round(sensor.relative_humidity,1)

        data = {"temp":f"{tempData1}","humi":f"{humiData1}"}
#       nydata = {
#    "device": {"device_id": str(device_id_0)},
#    "temp":f"{tempData1}",
#    "humi":f"{humiData1},
#    "timestamp": datetime.now().isoformat()}
        data1 = json.dumps(data)
        print(data1)
        
        print("Sending...")
        modem.send(data1.encode())
        
        print("Sent!")
        time.sleep(1)
      
'''
if __name__ == "__main__":
    main()
