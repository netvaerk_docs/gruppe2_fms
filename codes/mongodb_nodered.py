import pymongo
import socket 
import json
import re
from time import sleep
import random


HOST = "172.20.10.5"
PORT = 2000

dbName = "Målinger"
colName = "sensor_readings"
host = "mongodb://localhost:27017/"

client = pymongo.MongoClient(host)
mydb = client[dbName]
mycol = mydb[colName]

def send_data(data):
    clientsocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    clientsocket.connect((HOST, PORT))
    
    clientsocket.send(data.encode())
    #print("json:")
    #print(data)

def prepare_data(data):
    #print(data)
    data = str(data)
    data = data.replace("O", '"O')
    data = data.replace(")", ')"')
    data = data.replace("'", '"')
    data = data.replace('")', "')")
    data = data.replace('("', "('")

    print(data)
    print("")
    try:
        send_data(data)
    except:
        print("")
        print("No connection to Node-red...")
        print("")
    
def get_data():
    for x in mycol.distinct('device'):
        y = mycol.find_one({"device": x}, sort=[( '_id', pymongo.DESCENDING )])
        print(y)
        prepare_data(y)


while True:
    get_data()
    sleep(0.3)